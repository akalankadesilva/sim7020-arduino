#include "sim7020.h"

void sim7020::setDebug(Stream &serial,int level){
	DEBUG_SERIAL=&serial;
	debug=level;
}
unsigned long sim7020::getTime(){
	return millis();
}
int sim7020::readLine(char* line){
	int readLength=0;
	if((readLength=MODEM_SERIAL->available())>0){

		if(readLength+cmdBufferLength>CMD_BUFF_LEN){
			int overflow=readLength+cmdBufferLength-CMD_BUFF_LEN;
			shiftBuffer(cmdBuffer,overflow,-overflow,CMD_BUFF_LEN-overflow);
			cmdBufferLength=-overflow;
		}
		char* writePtr=cmdBuffer+cmdBufferLength;
		MODEM_SERIAL->readBytes(writePtr,readLength);
		cmdBufferLength+=readLength;
		cmdBuffer[cmdBufferLength]=0;
		if(debug>=DEBUG_RAW)
			for(int i=0;i<cmdBufferLength;i++){
				DEBUG_SERIAL->print(cmdBuffer[i],HEX);
				DEBUG_SERIAL->print(" ");
			}
	
	}
	if(cmdBufferLength>0){
 		char* ptr;
 		for(int i=0;i<cmdBufferLength;i++){
	      if(cmdBuffer[i]=='\r'){
	        ptr=cmdBuffer+i;
	        break;
	      }
	      if(i==cmdBufferLength-1)
	        return 0;
	    }
		
		int leLength=1;
		if(*(ptr+1)=='\n')
			leLength=2;
		int cmdLength=ptr-cmdBuffer;
		if(cmdBuffer[0]=='\n'){
	      strncpy(line,cmdBuffer+1,cmdLength-1);
	      cmdLength-=1;
	    }
	    else
	      strncpy(line,cmdBuffer,cmdLength);
		line[cmdLength]=0;
		if(debug>=DEBUG_AT)
			DEBUG_SERIAL->println(line);
		
		shiftBuffer(cmdBuffer,cmdLength+leLength,-cmdLength-leLength,CMD_BUFF_LEN-cmdLength-leLength);
		cmdBufferLength=cmdBufferLength-cmdLength-leLength;
		clearBuffer(cmdBuffer,cmdBufferLength,CMD_BUFF_LEN);
		return cmdLength;
		
		
	}
	return 0;
}
void sim7020::shiftBuffer(char* input,int start,int shift,int length){
	for(int i=0;i<length;i++){
		input[start+shift+i]=input[start+i];
	}
}
void sim7020::clearBuffer(char* input,int start,int end){
	for(int i=start;i<end;i++){
		input[i]=0;
	}
}


int sim7020::waitResponse(char* response,unsigned long timeout,int length){
  if(length==-1)
    length=strlen(response);
	
	unsigned long startTime=getTime();
	clearBuffer(lastResponse,0,LAST_RESP_LEN);
	int readLength=readLine(lastResponse);

	while(getTime()-startTime<timeout){
		if(readLength>0){
			if(strncmp(lastResponse,response,length)==0){
				return RESPONSE_COMPLETE;
			}
			else if(strcmp(lastResponse,"ERROR")==0){
				return RESPONSE_ERROR;
			}
			else{
				checkUrc(lastResponse);
			}

		}
		clearBuffer(lastResponse,0,LAST_RESP_LEN);
		readLength=readLine(lastResponse);

	}
	return RESPONSE_TIMEOUT;
}

int sim7020::charMap(char* input,int length,int base){
	long value;
	char buffer[length+1];
	memcpy(buffer,input,length);
	buffer[length]=0;
	char* p;
	return (int)strtol(buffer,&p,base);
	

}
int sim7020::receivePacket(int* socket,char* packet){
	clearBuffer(lastResponse,0,LAST_RESP_LEN);
	readLine(lastResponse);
	checkUrc(lastResponse);

	if(rxFlag){
		rxFlag=false;
		memcpy(packet,rxBuffer,lastRxLength);
		*socket=lastRxSocket;
		return lastRxLength;
	}
	return 0;
}
int sim7020::available(){
	clearBuffer(lastResponse,0,LAST_RESP_LEN);
	readLine(lastResponse);
	checkUrc(lastResponse);

	if(rxFlag){
		return lastRxLength;
	}
	return 0;
}
int sim7020::getLastRxSocket(){
	return lastRxSocket;
}
int sim7020::read(){
	if(lastRxLength<=0)
		return -1;
	int value=rxBuffer[0];
	shiftBuffer(rxBuffer,1,-1,RX_BUFF_LEN-1);
	rxBuffer[lastRxLength]=0;
	--lastRxLength;
	return value;
}
void sim7020::checkUrc(char* modemResponse){
	if(strncmp(modemResponse,"+CSONMI:",8)==0){
		int length=0;
		int socket=0;
		if(sscanf(modemResponse,"+CSONMI: %d,%d,",&socket,&length)==2){
			char* ptr=strchr(modemResponse,',');
			ptr=strchr(++ptr,',');
			++ptr;
			memset(rxBuffer,0,RX_BUFF_LEN);
			for(int i=0;i<length;i=i+2){
				rxBuffer[i/2]=(char)charMap(ptr+i,2,16);
			}
			lastRxSocket=socket;
			lastRxLength=length/2;
			rxFlag=true;
		}
	}
}

void sim7020::init(Stream &serial,int pin) {
  MODEM_SERIAL = &serial;
  pwrPin=pin;
  pinMode(pwrPin,OUTPUT);
}

bool sim7020::reboot() {
 if(powerOFF())
 	if(powerON())
 		return true;
 return false;
 
}
bool sim7020::powerON(){
	if(debug>=DEBUG_STATUS)
  					DEBUG_SERIAL->println("DEBUG: Powering ON..");
	MODEM_SERIAL->println("AT");
	if(waitResponse("OK",1000)==RESPONSE_COMPLETE)
		return true;
	digitalWrite(pwrPin,HIGH);
	delay(600);
	digitalWrite(pwrPin,LOW);
	delay(3000);
	MODEM_SERIAL->println("AT");
	if(waitResponse("OK",1000)==RESPONSE_COMPLETE)
		return true;
	return false;

}
bool sim7020::powerOFF(){
	if(debug>=DEBUG_STATUS)
  					DEBUG_SERIAL->println("DEBUG: Powering OFF..");
	MODEM_SERIAL->println("AT");
	if(waitResponse("OK",1000)==RESPONSE_TIMEOUT)
		return true;
	digitalWrite(pwrPin,HIGH);
	delay(1000);
	digitalWrite(pwrPin,LOW);
	delay(1500);
	MODEM_SERIAL->println("AT");
	if(waitResponse("OK",1000)==RESPONSE_TIMEOUT)
		return true;
	return false;
}

bool sim7020::sleep() {
  MODEM_SERIAL->println("AT+CFUN=0");
  return waitResponse("OK", 2000) == RESPONSE_COMPLETE;
}
bool sim7020::wake() {
  MODEM_SERIAL->println("AT+CFUN=1");
  return waitResponse("OK", 10000) == RESPONSE_COMPLETE;
}
bool sim7020::isRegistered() {
  MODEM_SERIAL->println("AT+CEREG?");
  if(waitResponse("+CEREG:", 2000,7)==RESPONSE_COMPLETE){
  	int i1=0;
  	int i2=0;
  	if(sscanf(lastResponse,"+CEREG: %d,%d",&i1,&i2)==2){
  		if(i2==1){
  			waitResponse("OK",5000);
  			return true;
  		}
  	}
  }
  return false;
}
bool sim7020::attachNB(){
	MODEM_SERIAL->println("AT+CGATT=1");
	return waitResponse("OK",3000)==RESPONSE_COMPLETE;
}
bool sim7020::registerNB(int mcc,int mnc) {
  char cmd[32];
  if(mcc!=-1&&mnc!=-1)
  	sprintf(cmd,"AT+COPS=1,2,\"%d%02d\",9",mcc,mnc);
  else
  	sprintf(cmd,"AT+COPS=0");
  
   MODEM_SERIAL->println(cmd);
   waitResponse("OK",10000);
   MODEM_SERIAL->println("AT+CEREG=0");
   waitResponse("OK",10000);
   MODEM_SERIAL->println("AT+CGATT=1"); 
   waitResponse("OK",10000);
   

   long start = millis();
   int response=-1;
  while (millis() - start < REG_NB_TIMEOUT) { 
  	Serial.print("Time" );Serial.println(millis());
   	Serial.println("Sending CEREG..");
     MODEM_SERIAL->println("AT+CEREG?");
     response=waitResponse("+CEREG:", 5000,7);
     Serial.print("Response: ");Serial.println(response);
	  if(response==RESPONSE_COMPLETE){
	  	int i1=0;
	  	int i2=0;
	  	if(sscanf(lastResponse,"+CEREG: %d,%d",&i1,&i2)==2){
	  		waitResponse("OK",5000);

	  		Serial.println("JP0");
	  		if(i2==1){
	  			 
	  			if(debug>=DEBUG_STATUS)
  					DEBUG_SERIAL->println("DEBUG: NB registered");
	  			return true;
	  		}
	  	}
	  }
	  else{
	  	if(debug>=DEBUG_STATUS)
  				DEBUG_SERIAL->println("DEBUG: Modem not responding");
	  	return false;
	  }
	  Serial.println("JP0.5");
    delay(500);
    delay(500);
    delay(500);
    Serial.println("JP1");
  }
  if(debug>=DEBUG_STATUS)
  				DEBUG_SERIAL->println("DEBUG: NB register failed");
  return false;
}
bool sim7020:: setFunction(unsigned char mode)
{
  MODEM_SERIAL->print(F("AT+CFUN="));
  MODEM_SERIAL->println(mode);
  return waitResponse("OK", 5000)==RESPONSE_COMPLETE;
}
bool sim7020:: getIMEI(char* imei)
{
  
  MODEM_SERIAL->println(F("AT+CGSN=1"));
  if(waitResponse("+CGSN:", 2000, 6)==RESPONSE_COMPLETE){
  	strcpy(imei,lastResponse+6);
  	return true;
  }
  return false;
}

bool sim7020:: getIMSI(char* imsi)
{
  
  MODEM_SERIAL->println(F("AT+CIMI"));
  waitResponse("AT+CIMI",5000);
  bool status=waitResponse("",5000);
  strcpy(imsi,lastResponse);
  return status;
}


bool sim7020:: setAPN(char* apn)
{
  char cmd[64];
  sprintf(cmd,"AT+CGDCONT=1,\"IP\",\"%s\"",apn);
  MODEM_SERIAL->println(cmd);
  return waitResponse("OK", 5000) == RESPONSE_COMPLETE;
}



bool sim7020::isPSMEnabled() {
  MODEM_SERIAL->println("AT+CPSMS?");
  if(waitResponse("+CPSMS:", 2000,7)==RESPONSE_COMPLETE){
  	int i1=0;
  	if(sscanf(lastResponse,"+CPSMS:%d",&i1)==1){
  		if(i1==1){
  			waitResponse("OK",5000);
  			return true;
  		}
  		else{
  			waitResponse("OK",5000);
  		}
  	}
  }
  return false;
}

uint16_t sim7020::getPSMTimeout() {

 int timeout=-1;
  MODEM_SERIAL->println("AT+CEREG=4");
  if(waitResponse("OK",5000)==RESPONSE_COMPLETE){
  	 MODEM_SERIAL->println("AT+CEREG?");
	  if(waitResponse("+CEREG:", 2000,7)==RESPONSE_COMPLETE){
	  	int i1,i2,i3,i4,i5,i6,i7;
	  	char timer[16];
	  	char* p;
	  	if(sscanf(lastResponse,"+CEREG: %d,%d,\"%x\",\"%x\",%d,\"%x\",,,\"%[^\"]\",\"%d\"",&i1,&i2,&i3,&i4,&i5,&i6,timer,&i7)==8){
	  		timeout=strtol(timer,&p,2);
	  		Serial.println(timeout);
	  		waitResponse("OK",5000); 
	  	}
	  }
	}
	MODEM_SERIAL->println("AT+CEREG=0");
	waitResponse("OK",5000);
	uint16_t base=timeout&0x1F;
	uint16_t scale=timeout/32;
	if(scale==0)
		base=base*2;
	else if(scale==1)
		base=base*60;
	else if(scale==2)
		base=base*180;
	else
		base=0;
	Serial.println(base);
  return base&0xFFFF;
}
bool sim7020::setPSM(bool enable,uint16_t activityTimeout) {


MODEM_SERIAL->println("AT+CPSMS=0");
 bool ret=waitResponse("OK",5000)==RESPONSE_COMPLETE;
if(!enable)
	return ret;

 int timeout1=-1;
 int timeout2=-1;
 char timer1[16];
 char timer2[16];
/*  MODEM_SERIAL->println("AT+CEREG=4");
  if(waitResponse("OK",5000)==RESPONSE_COMPLETE){
  	 MODEM_SERIAL->println("AT+CEREG?");
	  if(waitResponse("+CEREG:", 2000,7)==RESPONSE_COMPLETE){
	  	int i1,i2,i3,i4,i5,i6;
	  	
	  	char* p1;
	  	char* p2;
	  	if(sscanf(lastResponse,"+CEREG: %d,%d,\"%x\",\"%x\",%d,\"%x\",,,\"%[^\"]\",\"%[^\"]\"",&i1,&i2,&i3,&i4,&i5,&i6,timer1,timer2)==8){
	  		timeout1=strtol(timer1,&p1,2);
	  		timeout2=strtol(timer2,&p2,2);
	  		waitResponse("OK",5000); 
	  	},
	  }
	}

	MODEM_SERIAL->println("AT+CEREG=0");
	waitResponse("OK",5000);*/
	int base=0;
	int scale=0;
	int scaleFactor=0;
	if(activityTimeout<60){
		scale=2;
		scaleFactor=0;	
	}
	else if(activityTimeout>=60&&activityTimeout<360){
		scale=60;
		scaleFactor=1;
	}
	else if(activityTimeout>=360){
		scale=360;
		scaleFactor=2;
	}
	base=activityTimeout/scale;
	timeout2=(scaleFactor<<5)+base;
	
	itoa(timeout2,timer2,2);
	char newTimer2[16];
	int len=strlen(timer2);
	int idx=0;
	for(idx=0;idx<8-len;idx++){
		newTimer2[idx]=0x30;
	}
	strcpy(newTimer2+idx,timer2);
	newTimer2[8]=0;
	MODEM_SERIAL->print("AT+CPSMS=1,,,\"01000010\",\"");
	MODEM_SERIAL->print(newTimer2);
	MODEM_SERIAL->println("\"");
	return waitResponse("OK",5000)==RESPONSE_COMPLETE;
}

bool sim7020::isConnected(int socketId){
	MODEM_SERIAL->print("AT+CSOSTATUS=");
	MODEM_SERIAL->println(socketId);
	if(waitResponse("+CSOSTATUS:",5000,11)==RESPONSE_COMPLETE){
		int i1,i2=0;
		if(sscanf(lastResponse,"+CSOSTATUS: %d,%d",&i1,&i2)==2){

			if(i2==2){
				waitResponse("OK",5000);
				return true;
			}
		}
	}
	return false;
}
int sim7020::openTCPSocket(const char* host,int port){
	int socketId=-1;
	MODEM_SERIAL->println("AT+CSOC=1,1,1");
	if(waitResponse("+CSOC:",10000,6)==RESPONSE_COMPLETE){
		
		if(sscanf(lastResponse,"+CSOC: %d",&socketId)==1){
			MODEM_SERIAL->print("AT+CSOCON=0,");
			MODEM_SERIAL->print(port);
			MODEM_SERIAL->print(",\"");
			MODEM_SERIAL->print(host);
			MODEM_SERIAL->println("\"");
			if(waitResponse("OK",30000)==RESPONSE_COMPLETE){
				if(debug>=DEBUG_STATUS){
					DEBUG_SERIAL->print("DEBUG: TCP connected socket: ");
					DEBUG_SERIAL->println(socketId);
				}
  						
				return socketId;
			}
		}
		else{
			if(debug>=DEBUG_STATUS)
  				DEBUG_SERIAL->println("DEBUG: TCP connect failed");
		}
	}
	else{
		if(debug>=DEBUG_STATUS)
  				DEBUG_SERIAL->println("DEBUG: TCP init failed");
	}
	return socketId;
}
bool sim7020::sendTCPPacket(int socketId,char* packet,int length){
	MODEM_SERIAL->print("AT+CSOSEND=");
	MODEM_SERIAL->print(socketId);
	MODEM_SERIAL->print(",");
	MODEM_SERIAL->print(length*2);
	MODEM_SERIAL->print(",");
	for(int i=0;i<length;i++){
		MODEM_SERIAL->print(hexMap[packet[i]>>4]);
		MODEM_SERIAL->print(hexMap[packet[i]&0x0F]);
	}
	MODEM_SERIAL->println();
	if(waitResponse("OK",30000)==RESPONSE_COMPLETE){
		if(debug>=DEBUG_STATUS)
  				DEBUG_SERIAL->println("DEBUG: Packet Sent");
		return true;
	}
	return false;
}
bool sim7020::closeTCPSocket(int socketId){
	MODEM_SERIAL->print("AT+CSOCL=");
	MODEM_SERIAL->println(socketId);
	return waitResponse("OK",30000)==RESPONSE_COMPLETE;
}
int sim7020::openUDPSocket(const char* host,int port){
	int socketId=-1;
	MODEM_SERIAL->println("AT+CSOC=1,2,1");
	if(waitResponse("+CSOC:",10000,6)==RESPONSE_COMPLETE){
		
		if(sscanf(lastResponse,"+CSOC: %d",&socketId)==1){
			waitResponse("OK",30000);
			MODEM_SERIAL->print("AT+CSOCON=");
			MODEM_SERIAL->print(socketId);
			MODEM_SERIAL->print(",");
			MODEM_SERIAL->print(port);
			MODEM_SERIAL->print(",\"");
			MODEM_SERIAL->print(host);
			MODEM_SERIAL->println("\"");
			if(waitResponse("OK",30000)==RESPONSE_COMPLETE){
				if(debug>=DEBUG_STATUS)
  						DEBUG_SERIAL->println("DEBUG: UDP connected>");
				return socketId;
			}
		}
		else{
			if(debug>=DEBUG_STATUS)
  				DEBUG_SERIAL->println("DEBUG: UDP connect failed");
		}
	}
	else{
		if(debug>=DEBUG_STATUS)
  				DEBUG_SERIAL->println("DEBUG: UDP init failed");
	}
	return socketId;
}
bool sim7020::sendUDPPacket(int socketId,char* packet,int length){
	MODEM_SERIAL->print("AT+CSOSEND=");
	MODEM_SERIAL->print(socketId);
	MODEM_SERIAL->print(",");
	MODEM_SERIAL->print(length*2);
	MODEM_SERIAL->print(",");
	for(int i=0;i<length;i++){
		MODEM_SERIAL->print(hexMap[packet[i]>>4]);
		MODEM_SERIAL->print(hexMap[packet[i]&0x0F]);
	}
	MODEM_SERIAL->println();
	if(waitResponse("OK",30000)==RESPONSE_COMPLETE){
		if(debug>=DEBUG_STATUS)
  				DEBUG_SERIAL->println("DEBUG: Packet Sent");
		return true;
	}
	return false;
}
bool sim7020::closeUDPSocket(int socketId){
	MODEM_SERIAL->print("AT+CSOCL=");
	MODEM_SERIAL->println(socketId);
	return waitResponse("OK",30000)==RESPONSE_COMPLETE;
}
void sim7020::setRAI(){
	MODEM_SERIAL->println("AT+CNBIOTRAI=1");
	waitResponse("OK",5000);
}



