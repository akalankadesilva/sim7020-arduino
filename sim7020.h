#pragma once
#include <Stream.h>
#include <Arduino.h>
#define ENABLE_NB_TIMEOUT 25000
#define REG_NB_TIMEOUT 120000

#define RESPONSE_PENDING -1
#define RESPONSE_COMPLETE 1
#define RESPONSE_TIMEOUT 2
#define RESPONSE_ERROR 3

#define DEBUG_OFF 0
#define DEBUG_STATUS 1
#define DEBUG_AT 2
#define DEBUG_RAW 3

#define CMD_BUFF_LEN 512
#define LAST_RESP_LEN 512
#define RX_BUFF_LEN 512

class sim7020{
  public:
    
    void init(Stream &serial,int pin);
    void setDebug(Stream &serial,int level);
    unsigned long getTime();
    bool reboot();
    bool powerON();
    bool powerOFF();
    bool sleep();
    bool wake();
    bool isRegistered();
    bool registerNB(int mcc=-1,int mnc=-1);
    bool setFunction(unsigned char mode);
    bool attachNB();
    bool getIMEI(char* imei);
    bool getIMSI(char* imsi);
    bool setAPN(char* apn);
    bool isPSMEnabled();
    uint16_t getPSMTimeout();
    bool setPSM(bool enable,uint16_t activityTimeout);
    void setRAI();
    bool isConnected(int socketId);
    int openTCPSocket(const char* host,int port);
    bool sendTCPPacket(int socketId,char* packet,int length);
    int receivePacket(int* socket,char* packet);
    bool closeTCPSocket(int socketId);
    int openUDPSocket(const char* host,int port);
    bool sendUDPPacket(int socketId,char* packet,int length);
    bool closeUDPSocket(int socketId);
    int available();
    int getLastRxSocket();
    int read();
    
  private:
    int pwrPin;
    Stream* MODEM_SERIAL;
    Stream* DEBUG_SERIAL;
    char cmdBuffer[CMD_BUFF_LEN];
    int cmdBufferLength=0;
    char lastResponse[LAST_RESP_LEN];
    bool recvFlag=false;
    int debug=0;
    char hexMap[16]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    char rxBuffer[RX_BUFF_LEN];
    int lastRxSocket=0;
    int lastRxLength=0;
    bool rxFlag=false;
    int charMap(char* input,int length,int base);
    int readLine(char* line);
    void shiftBuffer(char* input,int start,int shift,int length);
    void clearBuffer(char* input,int start,int end);
    int waitResponse(char* response,unsigned long timeout,int length=-1);
    void checkUrc(char* modemResponse);
};
